﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager
{
    public class SaveTasks
    {
        public void AddDonwload(string downloadPath, string downloadFileName, DateTime downloadDateTime, string operationName = "Download")
        {
            using (var context = new DataContext())
            {
                Journal journal = new Journal();
                journal.OperationName = operationName;
                journal.DownloadPath = downloadPath;
                journal.DownloadFileName = downloadFileName;
                journal.dateTime = downloadDateTime;
                
                context.Journals.Add(journal);
                context.SaveChanges();
            }
        }

        public void AddMoveFolder(string movePathFrom, string movePathTo, DateTime moveDateTime, string operationName = "Move folder")
        {
            using (var context = new DataContext())
            {
                Journal journal = new Journal();
                journal.OperationName = operationName;
                journal.MovePathFrom = movePathFrom;
                journal.MovePathTo = movePathTo;
                journal.dateTime = moveDateTime;

                context.Journals.Add(journal);
                context.SaveChanges();
            }
        }

        public void AddSendEmail(string emailAddress, string emailHeader, string emailText, DateTime emailDateTime, string operationName = "Send e-mail")
        {
            using (var context = new DataContext())
            {
                Journal journal = new Journal();
                journal.OperationName = operationName;
                journal.EmailAddress = emailAddress;
                journal.EmailHeader = emailHeader;
                journal.EmailText = emailText;
                journal.dateTime = emailDateTime;

                context.Journals.Add(journal);
                context.SaveChanges();
            }
        }

    }
}
