﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TaskManager
{
    public class MoveFolder
    {
        public MainWindow MainWindow { get; }

        public MoveFolder(MainWindow mainWindow)
        {
            MainWindow = mainWindow;
        }

        public void FromTo(string pathFrom, string pathTo)
        {
            if(Directory.Exists(pathTo))
            {
                MessageBox.Show("Папка с таким именем уже существует!");
            }
            else
            {
                Directory.Move(pathFrom, pathTo);
                if(Directory.Exists(pathTo))
                {
                    MessageBox.Show("Done!");
                }
            }
        }
    }
}
