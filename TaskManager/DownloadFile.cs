﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace TaskManager
{
    public class DownloadFile 
    {
        public MainWindow MainWindow { get; }

        public DownloadFile(MainWindow mainWindow)
        {
            MainWindow = mainWindow;
        }
        public void GetFile(string path, string name)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    //http://qaru.site/questions/11083370/email-address-input-validation
                    client.DownloadProgressChanged += Download;
                    client.DownloadFileCompleted += new AsyncCompletedEventHandler(Done);
                    if (File.Exists(name))
                    {
                        File.Delete(name);
                        client.DownloadFileAsync(new System.Uri(path), name);
                    }
                    else
                    {
                        client.DownloadFileAsync(new System.Uri(path), name);
                    }
                }
                catch(UriFormatException ex)
                {
                    MessageBox.Show("Неверный адрес!");
                }
            }
        }

        private void Download(object sender, DownloadProgressChangedEventArgs e)
        {
            MainWindow.progressBar.Value = e.ProgressPercentage;
        }

        private void Done(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("Done!");
            MainWindow.progressBar.Visibility = Visibility.Hidden;
            MainWindow.downloadButton.Visibility = Visibility.Visible;
        }
    }
}
