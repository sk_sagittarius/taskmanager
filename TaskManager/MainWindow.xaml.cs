﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TaskManager
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();


            downloadPath.Visibility = Visibility.Hidden;
            downloadName.Visibility = Visibility.Hidden;
            downloadButton.Visibility = Visibility.Hidden;
            progressBar.Visibility = Visibility.Hidden;

            movePathFrom.Visibility = Visibility.Hidden;
            movePathTo.Visibility = Visibility.Hidden;
            moveButton.Visibility = Visibility.Hidden;

            emailAddress.Visibility = Visibility.Hidden;
            emailHeader.Visibility = Visibility.Hidden;
            emailText.Visibility = Visibility.Hidden;
            emailButton.Visibility = Visibility.Hidden;
        }

        private DateTime GetDateAndTime()
        {
            try
            {
                int year = calendar.SelectedDate.Value.Year;
                var month = calendar.SelectedDate.Value.Month;
                var day = calendar.SelectedDate.Value.Day;

                var hour = timePicker.SelectedTime.Value.Hour;
                var minute = timePicker.SelectedTime.Value.Minute;

                DateTime dateTime = new DateTime(year, month, day, hour, minute, 0);
                return dateTime;
                //MessageBox.Show(dateTime.ToString("dd.MM.yyyy hh:mm"));
            }
            catch (InvalidOperationException exception)
            {
                //MessageBox.Show("Select date and time!");
                //MessageBox.Show(DateTime.Now.ToString());
                return DateTime.Now;
            }
        }


        private void ComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            Hanlde();
        }

        private void Hanlde()
        {
            switch (comboBoxType.SelectedItem.ToString().Split(new string[] { ": " }, StringSplitOptions.None).Last())
            {
                case "Скачать файл":
                    //DonwloadFile();
                    FieldVisibility(1);
                    break;
                case "Переместить каталог":
                    FieldVisibility(2);
                    //MoveFile();
                    break;
                case "Отправить email":
                    FieldVisibility(3);
                    //SendEmail();
                    break;
            }
        }

        private void DownloadButtonClick(object sender, RoutedEventArgs e)
        {
            var dateTime = GetDateAndTime();
            var path = downloadPath.Text;
            if (downloadName.Text == "" || !downloadName.Text.Contains('.'))
            {
                MessageBox.Show("Необходимо ввести имя файла с расширением");
            }
            else
            {
                var name = downloadName.Text;
                downloadButton.Visibility = Visibility.Hidden;
                progressBar.Visibility = Visibility.Visible;
                DownloadFile downloadFile = new DownloadFile(this);
                downloadFile.GetFile(path, name);
                //SaveTasks saveTasks = new SaveTasks();
                //saveTasks.AddDonwload(path, name, dateTime);
            }
        }

        private void MoveButtonClick(object sender, RoutedEventArgs e)
        {
            var dateTime = GetDateAndTime();
            if (movePathFrom.Text == "" || movePathTo.Text == "")
            {
                MessageBox.Show("Нет пути");
            }
            else
            {
                var pathFrom = movePathFrom.Text;
                var pathTo = movePathTo.Text;
                MoveFolder moveFolder = new MoveFolder(this);
                moveFolder.FromTo(pathFrom, pathTo);
                //SaveTasks saveTasks = new SaveTasks();
                //saveTasks.AddMoveFolder(pathFrom, pathTo, dateTime);
            }
        }

        private void EmailButtonClick(object sender, RoutedEventArgs e)
        {
            var dateTime = GetDateAndTime();
            var emailTo = emailAddress.Text;
            var header = emailHeader.Text;
            var text = new TextRange(emailText.Document.ContentStart, emailText.Document.ContentEnd).Text;
            SendEmail sendEmail = new SendEmail(this);
            sendEmail.SendingEmail(emailTo, header, text);
            //SaveTasks saveTasks = new SaveTasks();
            //saveTasks.AddSendEmail(emailTo, header, text, dateTime);
        }

        private void FieldVisibility(int caseNumber)
        {
            switch(caseNumber)
            {
                case 1:
                    downloadPath.Visibility = Visibility.Visible;
                    downloadName.Visibility = Visibility.Visible;
                    downloadButton.Visibility = Visibility.Visible;

                    movePathFrom.Visibility = Visibility.Hidden;
                    movePathTo.Visibility = Visibility.Hidden;
                    moveButton.Visibility = Visibility.Hidden;

                    emailAddress.Visibility = Visibility.Hidden;
                    emailHeader.Visibility = Visibility.Hidden;
                    emailText.Visibility = Visibility.Hidden;
                    emailButton.Visibility = Visibility.Hidden;
                    break;
                case 2:
                    movePathFrom.Visibility = Visibility.Visible;
                    movePathTo.Visibility = Visibility.Visible;
                    moveButton.Visibility = Visibility.Visible;

                    downloadPath.Visibility = Visibility.Hidden;
                    downloadName.Visibility = Visibility.Hidden;
                    downloadButton.Visibility = Visibility.Hidden;
                    progressBar.Visibility = Visibility.Hidden;

                    emailAddress.Visibility = Visibility.Hidden;
                    emailHeader.Visibility = Visibility.Hidden;
                    emailText.Visibility = Visibility.Hidden;
                    emailButton.Visibility = Visibility.Hidden;
                    break;
                case 3:
                    emailAddress.Visibility = Visibility.Visible;
                    emailHeader.Visibility = Visibility.Visible;
                    emailText.Visibility = Visibility.Visible;
                    emailButton.Visibility = Visibility.Visible;

                    downloadPath.Visibility = Visibility.Hidden;
                    downloadName.Visibility = Visibility.Hidden;
                    downloadButton.Visibility = Visibility.Hidden;
                    progressBar.Visibility = Visibility.Hidden;

                    movePathFrom.Visibility = Visibility.Hidden;
                    movePathTo.Visibility = Visibility.Hidden;
                    moveButton.Visibility = Visibility.Hidden;
                    break;
                default:
                    downloadPath.Visibility = Visibility.Hidden;
                    downloadName.Visibility = Visibility.Hidden;
                    downloadButton.Visibility = Visibility.Hidden;
                    progressBar.Visibility = Visibility.Hidden;

                    movePathFrom.Visibility = Visibility.Hidden;
                    movePathTo.Visibility = Visibility.Hidden;
                    moveButton.Visibility = Visibility.Hidden;

                    emailAddress.Visibility = Visibility.Hidden;
                    emailHeader.Visibility = Visibility.Hidden;
                    emailText.Visibility = Visibility.Hidden;
                    emailButton.Visibility = Visibility.Hidden;
                    break;
            }
        }
    }
}
