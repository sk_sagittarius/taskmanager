﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager
{
    public class Journal
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string OperationName { get; set; }
        public DateTime dateTime { get; set; }
        public string Period { get; set; }

        // Download
        public string DownloadPath { get; set; } = "";
        public string DownloadFileName { get; set; } = "";

        // Move
        public string MovePathFrom { get; set; } = "";
        public string MovePathTo { get; set; } = "";

        // Email
        public string EmailAddress { get; set; } = "";
        public string EmailHeader { get; set; } = "";
        public string EmailText { get; set; } = "";
    }
}
